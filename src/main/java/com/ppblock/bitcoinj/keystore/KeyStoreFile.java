package com.ppblock.bitcoinj.keystore;

import com.ppblock.bitcoinj.crypto.CipherParams;

/**
 * keystore file
 * @author yangjian
 * @since 2018-12-06 下午1:08.
 */
public class KeyStoreFile {

	private String address;
	private String ciphertext;
	private String mac;
	private CipherParams cipherparams;
	private String id;
	private int version;

	public KeyStoreFile() {
	}


}
