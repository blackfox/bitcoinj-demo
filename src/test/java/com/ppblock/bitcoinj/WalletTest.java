package com.ppblock.bitcoinj;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ppblock.bitcoinj.crypto.Crypto;
import org.apache.commons.lang.StringUtils;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Utils;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.UnreadableWalletException;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.WalletProtobufSerializer;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 钱包测试
 * @author yangjian
 * @since 2018-12-04 上午10:14.
 */
public class WalletTest {

	static Logger logger = LoggerFactory.getLogger(WalletTest.class);
	// 钱包文件
	static final String WALLET_FILE = "test.wallet";

	/**
	 * 生成钱包
	 */
	@Test
	public void generateWallet() {
		Wallet wallet = new Wallet(TestNet3Params.get());
		logger.info("address: {}", wallet.currentReceiveAddress());
		logger.info("private key: {}", wallet.currentReceiveKey().getPrivateKeyAsHex());
		System.out.println(wallet.currentReceiveKey().getPrivateKeyAsWiF(getNetworkParameters()));
	}

	/**
	 * 生成助记词钱包
	 */
	@Test
	public void generateSeedWordWallet() {
		Wallet wallet = new Wallet(getNetworkParameters());
		// 备份助记词
		DeterministicSeed seed = wallet.getKeyChainSeed();
		logger.info("mnemonic code: {}", StringUtils.join(seed.getMnemonicCode(), " "));
		logger.info("address: {}", wallet.currentReceiveAddress());
		logger.info("private key: {}", wallet.currentReceiveKey().getPrivateKeyAsHex());
		// display same fog enroll crane address approve put among one float airport
		// mmtJZtdJJEpAyuLv8sb5R11yidGdJkoiLF
		// 796b52b7e530a191900f30ca26f696328348bca7e570198919a47e1f2b27bb8d
	}

	/**
	 * 导出钱包到文件
	 */
	@Test
	public void exportWalletToFile() throws IOException {
		final File walletFile = new File(WALLET_FILE);
		Wallet wallet = new Wallet(getNetworkParameters());

		logger.info("address: {}", wallet.currentReceiveKey().toAddress(getNetworkParameters()));
		logger.info("private key: {}", wallet.currentReceiveKey().getPrivateKeyAsHex());
		wallet.saveToFile(walletFile);
	}

	/**
	 * 从钱包文件加载钱包
	 */
	@Test
	public void loadWalletFromFile() throws FileNotFoundException, UnreadableWalletException {
		FileInputStream walletStream = new FileInputStream(WALLET_FILE);
		Wallet wallet = new WalletProtobufSerializer().readWallet(walletStream);
		logger.info("address: {}", wallet.currentReceiveAddress());
		// mfiMxvJJixR6x5wcBRPCLvDnV5Gp6Kkw9j
		logger.info("private key: {}", wallet.currentReceiveKey().getPrivateKeyAsHex());
		// d6c5ff41f407725f30a059f42f6828d387aace80572967c1052269af13d65fe3
	}

	/**
	 * 通过私钥恢复钱包
	 */
	@Test
	public void importWalletFromPrivateKey() {
		String privateKeyStr = "df9f4e73920f4155bf95dc5c6073979eb8bce0914a55b8d8f476e806957c3d1f";
		// address: micEb5rozW3oVoZpfFWdwkwT8VFeSJAfS9
		BigInteger privateKey = new BigInteger(privateKeyStr, 16);
		ECKey key = ECKey.fromPrivate(privateKey);

		logger.info("address: {}", key.toAddress(getNetworkParameters()).toBase58());
		logger.info("private key: {}", key.getPrivateKeyAsHex());
	}

	/**
	 * 通过 WIF 私钥恢复钱包
	 */
	@Test
	public void importWalletFromWIFPrivKey() {
		String wifPrivKey = "cVg1MLabhKRVCggaZW7g9AFoThJ8RKAUQSMRgVZmtgcyK3RYgzHj";
		// address: mrXpESJAUwUpzfjgCY6yGLpcD1tQNxMxZq
		ECKey key = DumpedPrivateKey.fromBase58(RegTestParams.get(), wifPrivKey).getKey();
		System.out.println(key.toAddress(RegTestParams.get()).toBase58());
	}

	/**
	 * 通过助记词恢复钱包
	 * @throws UnreadableWalletException
	 */
	@Test
	public void importWalletFromSeedWord() throws UnreadableWalletException {
		String seedWord = "paper change can prepare minor caution sun category cry find hope banner";
		// mma5tNxw9pKPxEGDEkg5iRpUnG3BWhRK8T
		// d8cf9070c0308362b5dd019166b7257a40b8e8178f4dc29e5c66e274ce524a55
		DeterministicSeed deterministicSeed = new DeterministicSeed(seedWord, null, "", Utils.currentTimeSeconds());
		Wallet wallet = Wallet.fromSeed(getNetworkParameters(), deterministicSeed);
		logger.info("address: {}", wallet.currentReceiveAddress().toBase58());
		logger.info("private key: {}", wallet.currentReceiveKey().getPrivateKeyAsHex());
	}

	/**
	 * 导出 keystore 钱包文件
	 */
	@Test
	public void exportKeystoreFile() throws IOException {
		ECKey key = new ECKey();
		String privateKey = key.getPrivateKeyAsHex();
		// 加密私钥
		String password = "123456";
		Crypto crypto = Crypto.createSCryptCrypto(password, privateKey.getBytes());
		// 写入 keystore 文件
		String walletFileName = getWalletFileName(key.toAddress(getNetworkParameters()).toBase58());
		File destination = new File("keystore");
		if (!destination.exists()) {
			destination.mkdir();
		}
		// 写入文件
		File walletFile = new File(destination, walletFileName);
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, true);
		objectMapper.writeValue(walletFile, crypto);
	}

	/**
	 * 导入 keystore 文件恢复钱包
	 */
	@Test
	public void importWalletFromKeystore() throws IOException {
		String fileName = "keystore/UTC--2018-12-06T04-05-14.612000000Z--mfrbJg7Fcr1Q5bQvA8PSwSTEQAHvWZ4XML.json";
		String password = "123456";
		File file = new File(fileName);
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, true);
		Crypto crypto = objectMapper.readValue(file, Crypto.class);
		byte[] decrypt = crypto.decrypt(password);
		BigInteger privateKey = new BigInteger(new String(decrypt), 16);
		ECKey key = ECKey.fromPrivate(privateKey);

		logger.info("address: {}", key.toAddress(getNetworkParameters()).toBase58());
		logger.info("private key: {}", key.getPrivateKeyAsHex());
	}

	/**
	 * 获取钱包文件名
	 * @param address
	 * @return
	 */
	private String getWalletFileName(String address) {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("'UTC--'yyyy-MM-dd'T'HH-mm-ss.nVV'--'");
		ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
		return now.format(format) + address + ".json";
	}

	/**
	 * 获取网络环境
	 * @return
	 */
	private NetworkParameters getNetworkParameters() {
		return RegTestParams.get();
	}
}
