package com.ppblock.bitcoinj;

import org.bitcoinj.core.*;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.SPVBlockStore;
import org.bitcoinj.wallet.Wallet;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 比特币交易测试
 * @author yangjian
 * @since 2018-12-06 下午2:57.
 */
public class TransactionTest {

	//发送交易
	@Test
	public void send() throws Exception {

		NetworkParameters params = RegTestParams.get();
		String wifPrivKey = "cQ6te6N8schTBd1YKzNG9jW73GYvft74Pfar3jCGHWBAg63Gkpca";
		// address: n4dTk38zh8gHSvPHFRHdw17hJmQg62tj2P
		ECKey key = DumpedPrivateKey.fromBase58(params, wifPrivKey).getKey();
		System.out.println(key.toAddress(params).toBase58());
		Wallet wallet = new Wallet(params);
		wallet.importKey(key);
		String recipientAddress = "mwkSRvAo4eJNPBvAdmX4HddFTJHyYqTkHE";
		String amount = "1";
		Address targetAddress  = Address.fromBase58(params, recipientAddress);
		// Do the send of 1 BTC in the background. This could throw InsufficientMoneyException.
		SPVBlockStore blockStore = new SPVBlockStore(params, getBLockFile());
		BlockChain chain = new BlockChain(params, wallet,blockStore);
		PeerGroup peerGroup = new PeerGroup(params, chain);
		peerGroup.connectToLocalHost();
		Wallet.SendResult result = wallet.sendCoins(peerGroup, targetAddress, Coin.parseCoin(amount));
		Transaction transaction = result.broadcastComplete.get();
		System.out.println(transaction.getHashAsString());


	}

	public static File getBLockFile(){
		File file = new File("/tmp/bitcoin-blocks");
		if(!file.exists()){
			try {
				boolean newFile = file.createNewFile();
				if(newFile){
					return file;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

	@Test
	public void test() throws BlockStoreException {
		String wifPrivKey = "cMk2QL4Z7nPzk7hLpTdENdeR92xw1T4YmVXN2nSDXeNtXBLi3HGm";
		NetworkParameters params  = RegTestParams.get();
		// address: n4dTk38zh8gHSvPHFRHdw17hJmQg62tj2P
		ECKey key = DumpedPrivateKey.fromBase58(params, wifPrivKey).getKey();
		System.out.println(key.toAddress(params).toBase58());
		Wallet wallet = new Wallet(params);
		wallet.importKey(key);
		File blockFile = new File("/tmp/bitcoin-blocks");
		SPVBlockStore blockStore = new SPVBlockStore(params, blockFile);

		BlockChain blockChain = new BlockChain(params, wallet, blockStore);
		PeerGroup peerGroup = new PeerGroup(params, blockChain);
		//peerGroup.addPeerDiscovery(new DnsDiscovery(params));
		peerGroup.addWallet(wallet);

		System.out.println("Start peer group");
		peerGroup.start();

		System.out.println("Downloading block chain");
		peerGroup.downloadBlockChain();
		System.out.println("Block chain downloaded");
		Coin balance = wallet.getBalance();
		List<TransactionOutput> unspents = wallet.getUnspents();
		System.out.println(unspents);
		System.out.println("balance: "+balance.getValue());
	}
}
