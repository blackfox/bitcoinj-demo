package com.ppblock.bitcoinj;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ppblock.bitcoinj.crypto.Crypto;
import com.ppblock.bitcoinj.crypto.EncPair;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * 加密测试
 * @author yangjian
 * @since 2018-12-06 上午10:53.
 */
public class CryptoTest {


	/**
	 * 加密私钥并生成 keystore 钱包文件
	 * @throws IOException
	 */
	@Test
	public void test() throws IOException {
		String privateKey = "d8cf9070c0308362b5dd019166b7257a40b8e8178f4dc29e5c66e274ce524a55";
		String password = "123456";
		Crypto crypto = Crypto.createSCryptCrypto(password, privateKey.getBytes());
		// 加密成 EncPair 对象
		EncPair encPair = crypto.deriveEncPair(password, privateKey.getBytes());
		byte[] bytes = crypto.decryptEncPair(password, encPair);
		System.out.println(encPair.getEncStr());
		System.out.println(crypto.getCiphertext());
		System.out.println(encPair.getNonce());
		System.out.println(new String(bytes));

		// 将加密对象转成 json
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, true);

		String keystoreContent = objectMapper.writeValueAsString(crypto);
		System.out.println(keystoreContent);
		// 写入文件
		File walletFile = new File("wallet.json");
		objectMapper.writeValue(walletFile, crypto);
	}
}
