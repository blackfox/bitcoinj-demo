package com.ppblock.bitcoinj;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import org.bitcoinj.core.*;
import org.bitcoinj.params.RegTestParams;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.SPVBlockStore;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletCoinsReceivedEventListener;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BitCoinHelloWorld implements WalletCoinsReceivedEventListener {

	@Test
	public void main() {
		BitCoinHelloWorld demo = new BitCoinHelloWorld();

		demo.run();
	}

	public void run() {
		try {
			init();

			System.out.println("Waiting for coins...");

			while (true) {
				Thread.sleep(20);
			}
		} catch (BlockStoreException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void init() throws BlockStoreException {
		NetworkParameters params  = RegTestParams.get();

		//ECKey key = new ECKey();
		String wifPrivKey = "cMk2QL4Z7nPzk7hLpTdENdeR92xw1T4YmVXN2nSDXeNtXBLi3HGm";
		// address: n4dTk38zh8gHSvPHFRHdw17hJmQg62tj2P
		ECKey key = DumpedPrivateKey.fromBase58(params, wifPrivKey).getKey();
		System.out.println("We created a new key:\n" + key);

		Address addressFromKey = key.toAddress(params);
		System.out.println("Public Address generated: " + addressFromKey);

		System.out.println("Private key is: " + key.getPrivateKeyEncoded(params).toString());

		Wallet wallet = new Wallet(params);
		wallet.importKey(key);

		File blockFile = new File("/tmp/bitcoin-blocks");
		SPVBlockStore blockStore = new SPVBlockStore(params, blockFile);

		BlockChain blockChain = new BlockChain(params, wallet, blockStore);
		PeerGroup peerGroup = new PeerGroup(params, blockChain);
		//peerGroup.addPeerDiscovery(new DnsDiscovery(params));
		peerGroup.addWallet(wallet);

		System.out.println("Start peer group");
		peerGroup.start();

		System.out.println("Downloading block chain");
		peerGroup.downloadBlockChain();
		System.out.println("Block chain downloaded");

		wallet.addCoinsReceivedEventListener(this);
	}


	@Override
	public void onCoinsReceived(final Wallet wallet, final Transaction transaction, Coin prevBalance, Coin newBalance) {
		final Coin value = transaction.getValueSentToMe(wallet);

		System.out.println("Received tx for " + value.toFriendlyString() + ": " + transaction);

		System.out.println("Previous balance is " + prevBalance.toFriendlyString());

		System.out.println("New estimated balance is " + newBalance.toFriendlyString());

		System.out.println("Coin received, wallet balance is :" + wallet.getBalance());

		Futures.addCallback(transaction.getConfidence().getDepthFuture(1), new FutureCallback<TransactionConfidence>() {
			public void onSuccess(TransactionConfidence result) {
				System.out.println("Transaction confirmed, wallet balance is :" + wallet.getBalance());
			}

			public void onFailure(Throwable t) {
				t.printStackTrace();
			}
		});
	}
}
