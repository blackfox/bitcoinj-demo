package com.ppblock.bitcoinj;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yangjian
 * @since 2018-12-07 上午10:55.
 */
public class JsonRpcTest {

	JsonRpcHttpClient client;

	@Before
	public void setUp() throws MalformedURLException {
		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("user", "password".toCharArray());
			}
		});
		// 实例化客户端
		client = new JsonRpcHttpClient(new URL("http://127.0.0.1:8332"));
	}

	/**
	 * 获取余额
	 * @throws Throwable
	 */
	@Test
	public void getBalance() throws Throwable {

		Object [] params = new Object[3];
		params[0] = "*";
		params[1] = 1;
		params[2] = true;
		Map<String,String> headers = new HashMap<>(8);
		BigDecimal balance = client.invoke("getbalance", params, BigDecimal.class, headers);
		System.out.println(balance);

	}
}
